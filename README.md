Installing MySql 5.7+ on Raspberry Pi 3
----------------------------------------
The Below mentioned files are also available on the git


sudo wget http://ftp.debian.org/debian/pool/main/m/mysql-5.7/libmysqlclient-dev_5.7.17-1_armhf.deb
sudo wget http://ftp.debian.org/debian/pool/main/m/mysql-5.7/libmysqlclient20_5.7.17-1_armhf.deb
sudo wget http://ftp.debian.org/debian/pool/main/m/mysql-5.7/libmysqld-dev_5.7.17-1_armhf.deb
sudo wget http://ftp.debian.org/debian/pool/main/m/mysql-5.7/mysql-client-5.7_5.7.17-1_armhf.deb
sudo wget http://ftp.debian.org/debian/pool/main/m/mysql-5.7/mysql-client-core-5.7_5.7.17-1_armhf.deb
sudo wget http://ftp.debian.org/debian/pool/main/m/mysql-5.7/mysql-server-5.7_5.7.17-1_armhf.deb
sudo wget http://ftp.debian.org/debian/pool/main/m/mysql-5.7/mysql-server-core-5.7_5.7.17-1_armhf.deb
sudo wget http://ftp.debian.org/debian/pool/main/m/mecab/libmecab2_0.996-3_armhf.deb
sudo wget http://ftp.debian.org/debian/pool/main/m/mysql-defaults/mysql-common_5.8+1.0.2_all.deb
sudo wget http://ftp.debian.org/debian/pool/main/l/lz4/liblz4-1_0.0~r131-2+b1_armhf.deb
sudo apt install libaio1 libaio-dev libhtml-template-perl libevent-core-2.0-5
#Install GC++,GC 6
* Extract the archive, it is a .tar file compressed with 7zip, you will need to install p7zip-full on your Raspberry Pi.
* sudo apt-get install p7zip-full
* Copy/move gcc-6.1.0 to /usr/local/gcc-6.1.0
* Add the above to your path:
```
#!console
export PATH=/usr/local/gcc-6.1.0/bin:$PATH
```
* Now, you should be able to use the compilers: *gcc-6.1.0* for C and *g++-6.1.0* for C++.
* If you want to permanently add the compilers to your path, append the *export* to the end of your .profile file.
sudo dpkg -i libmecab2_0.996-3_armhf.deb
sudo dpkg -i liblz4-1_0.0~r131-2+b1_armhf.deb
sudo dpkg -i mysql-common_5.8+1.0.2_all.deb
sudo dpkg -i mysql-client-core-5.7_5.7.17-1_armhf.deb
sudo dpkg -i mysql-client-5.7_5.7.17-1_armhf.deb
sudo dpkg -i mysql-server-core-5.7_5.7.17-1_armhf.deb
sudo dpkg -i mysql-server-5.7_5.7.17-1_armhf.deb